<?php

namespace App\Console;

use App\Console\Commands\SendEmail;
use App\Http\Controllers\simpleController;
use App\Jobs\SimpleJob;
use Exception;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Stringable;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /* Defining Schedule */
        // 1. Closure
        // $schedule->call(function () {
        //     info("Closure called");
        // }); 

        /* 2. Invokable objects */
        // $schedule->call(new simpleController);

        /* 3. Artisan Commands */
        // command's name
        // $schedule->command('emails:send');
        // command's class
        // $schedule->command(SendEmail::class);

        /* 4. Queued Jobs */
        // $schedule->job(new SimpleJob);

        // 5. Shell Commands
        // $schedule->exec('node /home/forge/script.js');
        
        /* Frequency options */
        /*
            -------------> minutes(0-59)
            | -----------> hour (0-23)
            | | ---------> day of month (1-31)
            | | | -------> month (1-12)
            | | | | -----> day of week (0-6) (Sunday is 0)
            | | | | |
            * * * * * <command to execute>
         */
        // $schedule->call(function () {
        //     info('Run once per week on Monday at 1 PM');
        // })->weekly()->mondays()->at('13:00');

        // $schedule->command('emails:send')
        //     ->cron('0 * * * 1-5');
        //     ->hourly()
        //     ->weekdays()
        //     ->between('8:00', '17:00');

        /* Truth contraints */
        // $schedule->command('emails:send')
        //     ->when(function () {
        //         return true;
        //     })
        //     ->skip(function () {
        //         return true;
        //     });

        /* Environment Constraints */
        // $schedule->command('emails:send')
        //     ->environments(['staging', 'production']);
   
        /* Background Tasks */
        // $schedule->command('emails:send')
        //     ->runInBackground();

        /* Preventing Task Overlaps */
        // $schedule->command('emails:send')
        //     ->withoutOverlapping();

        /* Running Tasks On One Server */
        // $schedule->command('emails:send')
        //     ->onOneServer();

        /* Task hooks */
        // $schedule->command('emailssend')
        //     ->before(function () {
        //         info("Before task executed");
        //     })
        //     ->after(function () {
        //         info("After task executed");
        //     })
        //     ->onSuccess(function () {
        //         info("Task succeeded");
        //     })
        //     ->onFailure(function () {
        //         info("Task failed");
        //     });

        /* Task Output */
        // $schedule->command('emails:send')
        // ->sendOutputTo(storage_path('/logs/result.log'));
        // ->appendOutputTo(storage_path('/logs/result.log'));
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}


//  schedule:run 
// Artisan command will evaluate all of your scheduled tasks and determine if they need to run based on the server's current time.

// -------------> minutes(0-59)
// | -----------> hour (0-23)
// | | ---------> day of month (1-31)
// | | | -------> month (1-12)
// | | | | -----> day of week (0-6) (Sunday is 0)
// | | | | |
// * * * * * <command to execute>